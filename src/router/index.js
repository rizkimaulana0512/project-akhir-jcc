import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "about" */ '../views/Home.vue')
  },
  {
    path: '/blogs',
    name: 'Blogs',
    component: () => import(/* webpackChunkName: "Blogs" */ '../views/Blogs.vue')
  },
  {
    path: '/blog/:id',
    name: 'Blog',
    component: () => import(/* webpackChunkName: "Blogs" */ '../views/Blog.vue')
  },
  {
    path: '/register',
    name: 'register',
    beforeEnter :(to,from,next)=>{
      let token = localStorage.getItem('authentication')
      if(token === `""`){
        next()
      } else {
          next('/')
      }
    },
    component: () => import(/* webpackChunkName: "Blogs" */ '../views/Register.vue')
  },
  {
    path: '/addBlog',
    name: 'addBlog',
    beforeEnter :(to,from,next)=>{
      let token = localStorage.getItem('authentication')
      if(token === `""`){
        next('/')
      } else {
          next()
      }
    },
    component: () => import(/* webpackChunkName: "Blogs" */ '../views/AddBlog.vue')
  },
  {
    path: '/editBlog/:id',
    name: 'editBlog',
    beforeEnter :(to,from,next)=>{
      let token = localStorage.getItem('authentication')
      if(token === `""`){
        next('/')
      } else {
          next()
      }
    },
    component: () => import(/* webpackChunkName: "Blogs" */ '../views/EditBlog.vue')
  },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
