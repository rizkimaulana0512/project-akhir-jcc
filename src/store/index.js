import Vue from 'vue'
import Vuex from 'vuex'
import alert from './alert'
import dialog from './dialog'
import auth from './auth'
import VuexPersist from 'vuex-persist'

const vuexPersist = new VuexPersist({
    key : 'sanbercode',
    storage : localStorage,
})
const authentication = new VuexPersist({
    key : 'authentication',
    storage : localStorage,
    reducer : (state) => (state.auth.token)
})
Vue.use(Vuex)

export default new Vuex.Store({
    plugins : [vuexPersist.plugin,authentication.plugin],
    modules : {
        alert,
        dialog,
        auth
    }
})  